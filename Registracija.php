<?php


session_start();
error_reporting(0);
$status = $_SESSION["prijavljen"];


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Biblioteka</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>


    </head>
    <body>
    <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="index.php">Biblioteka</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Pocetna <span class="sr-only">(current)</span></a></li>
      </ul>
      <form class="navbar-form navbar-left" action="pretraga-db.php" method="get">
        <div class="form-group">
          <input type="text" class="form-control" name="pretraga" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Pretrazi</button>
      </form>
       
  
      <ul class="nav navbar-nav navbar-right">
          
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
               <?php
          if($status == false){
            echo "<li><a href = \"Logovanje.php\">Logovanje</a></li>";
            echo "<li><a href = \"Registracija.php\">Registracija</a></li>";
            echo"</ul>";
            echo"</li>";
     
           
          }else{
                echo "<li><a href = \"odjava.php\">Odjavi se</a></li>";    
          }   ?> 
      
          </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <div class="col-xs-4 col-xs-offset-4 okvir" id="sing_up">
       <div class="panel panel-default text-center">
                <div class="panel-heading">
                    
                   <h3>Registracija</h3>
                </div>
            </div>
     <form method="post" action="registracija-db.php">
      
      <h3>Unesite svoje osnovne podatke</h3>
      
      <input value="text" name="korisnici_id" hidden="hidden"/>
      <div class="form-group">
      <label for="korisnickoIme">Korisničko ime:</label>    
      <input  type="text" name="korisnickoIme" class="form-control" value="" placeholder="Vaše korisničko ime" id="korisnickoime"  pattern=".{6,}" required="required">
      </div>
      <div class="form-group">
      <label for="email">email:</label>    
      <input type="email" name="email" value="" class="form-control" placeholder="Vaša email adresa" id="mejl" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}" required="required">
      </div>
      <div class="form-group">
      <label for="lozinka">lozinka:</label>     
      <input type="password" name="lozinka" class="form-control" value="" placeholder="Vaša lozinka" id="lozinka" required="required" pattern=".{6,}">
      </div>
      <div class="form-group">
      <label for="Potvrda Lozinke">Potvrda Lozinke:</label>    
      <input type="password" name="potvrda_lozinke"  class="form-control" value="" placeholder="Potvrdite lozinku" id="potvrdalozinke"  required="required">
      </div>
   
                 <button type="submit" class="btn btn-default btn-lg center-block">Registracija</button>
      </form>
   
      <br>
   
 

   <br>
    </div>
        
        
        
    </body>
</html>
